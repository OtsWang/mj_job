import sqlite3
import os

# 配置
db_name = "./test.db"
src_name = "./source.txt"
result_file = "./result.csv"
target_dir = "./target"
target_file_format = ".xlsx"

# 数据库连接
connect = sqlite3.connect(db_name)
cur = connect.cursor()


def create_table():
    '''创建表'''

    create_sql = """create table test(
    mobile varchar not null,
    idx int not null,
    net_idx int not null,
    CONSTRAINT pIndex PRIMARY KEY (mobile)
    );"""
    cur.executescript(create_sql)
    print("create db ok")


def insert_data():
    '''插入数据：将源文件中的数据插入到数据库中'''

    delete_sql = "delete from test"
    cur.executescript(delete_sql)
    with open(src_name) as input_file:
        while True:
            line = input_file.readline()
            if not line:
                break
            strs = line.split(',')
            mobile = strs[1].replace('"','')
            idx = strs[2]
            net_idx = strs[0]
            cur.execute(f"insert or ignore into test values({mobile},{idx},{net_idx})")

    connect.commit()
    print("insert ok")


def get_idx(mobile):
    '''根据手机号查询数据库'''

    mobile = str(mobile).replace('"', '')
    cur.execute(f"select * from test where mobile = {mobile}")
    idx = 0
    net_idx = 0
    for x in cur:
        idx = x[1]
        net_idx = x[2]
        mobiles = [mobile]
        net_idxs = [net_idx]
        cur.execute(f"select * from test where idx = {idx} and mobile != {mobile}")
        for y in cur:
            mobile = y[0]
            net_idx = y[2]
            mobiles.append(mobile)
            net_idxs.append(net_idx)
        result = {'idx': idx, 'mobiles': mobiles, 'net_idxs': net_idxs}
        return result



def get_target_mobiles():
    '''需要匹配的手机号：read from files'''

    import openpyxl
    target_mobiles = set()
    file_names = os.listdir(target_dir)
    for file_name in file_names:
        path = os.path.join(target_dir, file_name)
        if os.path.isfile(path) and path.endswith(target_file_format):
            book = openpyxl.load_workbook(path)
            data = book.worksheets[0]['A']
            for cell in data[1:]:
                target_mobiles.add(cell.value)
    print(f'查询到待匹配手机号共：{len(target_mobiles)}')
    return target_mobiles;


def write_result(map_list):
    '''将结果写入到输出文件中'''

    file = open(result_file, 'w')
    header = f"网,号,ID,备注,内容\r\n"
    file.write(header)
    for tmp in map_list:
        idx = tmp['idx']
        mobiles = tmp['mobiles']
        net_idxs = tmp['net_idxs']
        for (index,mobile) in enumerate(mobiles):
            mark = " "
            msg = f"{mobile}"
            net_idx = net_idxs[index]
            if index == 0:
                mark = "*"
                msg = "/"
            line = f"{net_idx},{mobile},{idx},{mark},{msg}\r\n"
            file.write(line)
    file.flush()


if __name__ == '__main__':
 
    #create_table()
    #insert_data()
    target_mobiles = get_target_mobiles()
    map_list = []
    for target in target_mobiles:
        result = get_idx(target)
        if result:
            map_list.append(result)
    write_result(map_list)
    cur.close()
    connect.close()




