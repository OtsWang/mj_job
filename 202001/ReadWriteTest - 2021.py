import xlrd
import xlwt
from openpyxl.reader.excel import load_workbook

fileOutputName = "./2021行业短信财报收入分地市细表.xlsx"
sheetname = "通信主业收入明细表(过渡方案）"
year = 2021
month = 9
rowName1 = "7.2.2"
rowName2 = "7.3.2"
rowName3 = "7.2.12"
rowName4 = "7.3.5"
# row1 = row1 + row3
# row2 = row2 + row4

inColName1 = "上年同期数"
inColName2 = "本月发生数"
inColName3 = "本年累计数"

outinColName1 = "上年同期累计"
outinColName2 = "{}月".format(month)
outinColName3 = "本年累计"
        
citys = ["合计","福州","厦门","宁德","莆田","泉州","漳州","龙岩","三明","南平"]


def Num(num):
       if num == "":
               return 0
       return num

def getOutIndex(fileName, city, month):
        outbook=xlrd.open_workbook(fileName)
        sh=outbook.sheet_by_index(0)
        cols = sh.ncols
        rows = sh.nrows
        
        outRowIndexS = 0
        outColIndex1 = 0
        outColIndex2 = 0
        outColIndex3 = 0
        
        for r in range(rows):
                rowName = sh.cell_value(rowx=r, colx=0)
                if rowName == city:
                        outRowIndexS = r
        
        for c in range (cols):
                colName = sh.cell_value(rowx=1, colx=c)
                if colName == outinColName1:
                        outColIndex1 = c
                if colName == outinColName2:
                        outColIndex2 = c
                if colName == outinColName3:
                        outColIndex3 = c
        return outRowIndexS, chr(outColIndex1+ord('A')), chr(outColIndex2+ord('A')), chr(outColIndex3+ord('A'))
        #return outRowIndexS, outColIndex1, outColIndex2, outColIndex3
        
def getInputValue(filename, sheetname):
        book=xlrd.open_workbook(filename)
        sh=book.sheet_by_name(sheetname)
        cols = sh.ncols
        rows = sh.nrows
        
        rowIndex1 = 0
        rowIndex2 = 1
        rowIndex3 = 2
        rowIndex4 = 3
        colIndex1 = 0
        colIndex2 = 1
        colIndex3 = 2
        
        print(filename)
        for c in range (cols):
                colName = sh.cell_value(rowx=0, colx=c)
                if colName == inColName1:
                        colIndex1 = c
                if colName == inColName2:
                        colIndex2 = c
                if colName == inColName3:
                        colIndex3 = c
        for r in range(rows): 
                rowName = sh.cell_value(rowx=r,colx=0)
                if rowName == rowName1:
                        rowIndex1 = r
                if rowName == rowName2:
                        rowIndex2 = r
                if rowName == rowName3:
                        rowIndex3 = r
                if rowName == rowName4:
                        rowIndex4 = r
                        
        value11 = Num(sh.cell_value(rowx=rowIndex1, colx=colIndex1))
        value12 = Num(sh.cell_value(rowx=rowIndex1, colx=colIndex2))
        value13 = Num(sh.cell_value(rowx=rowIndex1, colx=colIndex3))
        
        value21 = Num(sh.cell_value(rowx=rowIndex2, colx=colIndex1))
        value22 = Num(sh.cell_value(rowx=rowIndex2, colx=colIndex2))
        value23 = Num(sh.cell_value(rowx=rowIndex2, colx=colIndex3))

        value31 = Num(sh.cell_value(rowx=rowIndex3, colx=colIndex1))
        value32 = Num(sh.cell_value(rowx=rowIndex3, colx=colIndex2))
        value33 = Num(sh.cell_value(rowx=rowIndex3, colx=colIndex3))
        
        value41 = Num(sh.cell_value(rowx=rowIndex4, colx=colIndex1))
        value42 = Num(sh.cell_value(rowx=rowIndex4, colx=colIndex2))
        value43 = Num(sh.cell_value(rowx=rowIndex4, colx=colIndex3))

        value11 = value11 + value31
        value12 = value12 + value32
        value13 = value13 + value33

        value21 = value21 + value41
        value22 = value22 + value42
        value23 = value23 + value43
        
        value01 = value11 + value21
        value02 = value12 + value22
        value03 = value13 + value23
        
        print(value01, end="\t\t")
        print(value02, end="\t\t")
        print(value03, end="\n")
        
        print(value11, end="\t\t\t")
        print(value12, end="\t\t\t")
        print(value13, end="\n")
        
        print(value21, end="\t\t\t\t")
        print(value22, end="\t\t\t\t")
        print(value23, end="\n")
        
        return value01, value02, value03, value11, value12, value13, value21, value22, value23

fileNameFormat = "./{month}月/A350{index}-福建股份﹒{city}_{year}年{month}月_{year}年财务月报.xls"
index = 0
workbook = load_workbook(fileOutputName)
worksheet = workbook.worksheets[0]
for city in citys:
        filename = fileNameFormat.format(index=index,city=city, year=year, month=month)
        values = getInputValue(filename, sheetname)
        index = index + 1
        outIndex = getOutIndex(fileOutputName, city, month)
        print(outIndex)
        
        # 写入excel
        worksheet[outIndex[1]+str(outIndex[0]+1)] = values[0]
        worksheet[outIndex[2]+str(outIndex[0]+1)] = values[1]
        worksheet[outIndex[3]+str(outIndex[0]+1)] = values[2]
        
        worksheet[outIndex[1]+str(outIndex[0]+2)] = values[3]
        worksheet[outIndex[2]+str(outIndex[0]+2)] = values[4]
        worksheet[outIndex[3]+str(outIndex[0]+2)] = values[5]
        
        worksheet[outIndex[1]+str(outIndex[0]+3)] = values[6]
        worksheet[outIndex[2]+str(outIndex[0]+3)] = values[7]
        worksheet[outIndex[3]+str(outIndex[0]+3)] = values[8]

# 保存
# workbook.save("./{}行业短信财报收入分地市细表{}.xlsx".format(year,month))
workbook.save(fileOutputName)

